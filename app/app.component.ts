import { Component } from 'angular2/core';
import { Bookmark } from './bookmark';
import { BookmarkService } from './bookmark.service';
@Component({
    selector: 'my-app',
    template: `
    <h1>My First Angular 2 App</h1>

    <ul>
        <li *ngFor="#bookmark of bookmarks">
            <b>{{bookmark.title}}</b> : <i>{{bookmark.url}}</i>
        </li>
    </ul>
    `,
    providers: [BookmarkService]
})
export class AppComponent {
    bookmarks: Bookmark[];
    errorString: string;

    constructor(private _bookmarkService: BookmarkService) {

    }

    ngOnInit() {
        this.getBookmarks();
    }

    getBookmarks() {
        this._bookmarkService.getBookmarks()
                               .subscribe(
                                   bookmarks => this.bookmarks = bookmarks,
                                   error => this.errorString = <any> error
                               );
    }
}